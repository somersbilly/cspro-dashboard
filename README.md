CSPro dashboard is a Nextjs dashboard built by the Census Processing. The app is used for monitoring progress of field data collection using CSEntry, the android app developed with CSPro. See more from the US Census Bureau on how to develop Computer Assisted Personal Interview (CAPI).

The app is not intended to replace CSWeb, but to simplify visualisations and interpretations.

## Getting Started

```
git clone https://github.com/billyws/cspro-dashboard.git
```

```
npm install
npm run dev
```
Go to http://localhost:3000

## Deployment
The application will be deployed to AWS

## Built with
- [Nextjs](https://nextjs.org/)
- Nescafe Coffee